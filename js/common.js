function getParameterByName(name, url = window.location.href) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function getInvestorImage(investorimage) {
  try {
    var imgs = JSON.parse(investorimage);
    return "./uploads/" + imgs[0].serverFileName;
  } catch (error) {
    return "";
  }
}


window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
    document.querySelector(".nav").style.transition = "padding 0.2s, opacity 0.2s, height 0.2s, background 0.2s";
    document.querySelector(".nav").style.background = "#ABC6F7";
    // document.querySelector(".nav").style.height = "75px";
    document.querySelector(".nav").style.opacity = "0.7";
    // document.querySelector(".nav").style.padding = "0px 15px";
    document.querySelector(".nav").classList.add = "nav-after-scroll";
    // document.querySelector(".nav-brand-logo").style.height = "22.5px";
    // document.querySelector(".nav-brand-logo").style.width = "79px";
  } else {
    document.querySelector(".nav").style.transition = "padding 0.2s, opacity 0.2s, height 0.2s, background 0.2s";
    document.querySelector(".nav").style.background = "#e8edf6";
    // document.querySelector(".nav").style.height = "88px";
    document.querySelector(".nav").style.opacity = "1";
    // document.querySelector(".nav").style.padding = "0px 45px";
    document.querySelector(".nav").classList.remove = "nav-after-scroll";
    // document.querySelector(".nav-brand-logo").style.height = "45.2px";
    // document.querySelector(".nav-brand-logo").style.width = "158px";
  }
} 

/* Set the width of the sidebar to 250px (show it) */
function openLeftNav() {
  document.getElementById("mySidepanel").style.width = "350px";
}

/* Set the width of the sidebar to 0 (hide it) */
function closeLeftNav() {
  document.getElementById("mySidepanel").style.width = "0";
} 